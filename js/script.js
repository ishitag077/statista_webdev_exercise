console.log('External script implemented successfully'); // Just for debugging, delete later


let fizzBuzzList = document.getElementById("fizzBuzzList");
let h2 = document.createElement("h2");
h2.innerText = "FizzBuzz List";
h2.classList.add("fizzBuzzList__h2text");
fizzBuzzList.append(h2);
let ul = document.createElement("ul");
ul.style.display = "none";
ul.classList.add("fizzBuzzList__ul")
let fizzBuzzButton = document.createElement("button");
fizzBuzzButton.innerText = "Show List";
fizzBuzzButton.classList.add("fizzBuzzList__fizzbutton")
const showValue = (value) =>{

    let li = document.createElement("li");
   
     li.innerHTML = value;
     li.classList.add("fizzBuzzList__ul_li")
     let span = document.createElement("span");
     li.append(span);


    ul.append(li);
}



for ( let i = 1;  i<=100 ; i++){
    if  ( i %3 === 0 && i % 5 === 0){
        showValue("FizzBuzz")
    }
   else if(i % 5 === 0){
        showValue("Buzz")
    }
   else if ( i % 3 === 0) {
    showValue("Fizz")
    }
  else showValue(i)

}


fizzBuzzButton.addEventListener("click",()=>{
    if(ul.style.display === "none"){
        ul.style.display = "grid";
        fizzBuzzButton.innerHTML = "Hide List"
    }
    else{
       ul.style.display = "none";
        fizzBuzzButton.innerHTML = "Show List"
    }
})

fizzBuzzList.append(fizzBuzzButton);
fizzBuzzList.append(ul);

